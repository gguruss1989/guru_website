---
# Display name
title: Guru Singh

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Software Developer

# Organizations/Affiliations
organizations:
- name: Revenue Management Solutions
  url: "https://revmansolutions.com/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.


education:
  courses:
  - course: MS in Applied Mathematics & Computer Science
    institution: University of Central Oklahoma
    year: 2017
  - course: BE in Computer Engineering
    institution: New Horizon College of Engineering
    year: 2013

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: github
  icon_pack: fab
  link: https://github.com/gguruss
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/gguruss1989
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

Experienced software developer with a demonstrated history of working in various areas of healthcare, banking
and education industry. Skilled in Java C#, Javascript, Agile Methodologies and Databases. Strong information technology
professional with a Master’s Degree focused in Mathematics and Computer Science from University of Central Oklahoma.
