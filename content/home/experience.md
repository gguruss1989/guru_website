+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Java Developer"
  company = "Revenue Management Solutions LLC"
  company_url = "https://revmansolutions.com"
  location = "Oklahoma City"
  date_start = "2018-03-16"
  date_end = ""
  description = """
  Revenue Management Solutions provides process automation technology for healthcare providers. RMS offers best-in-class automation solutions that integrate with all major healthcare billing systems and commercial banking partners to maximize automation.

  Responsibilities include:
  * Create applications that parse and extract information from healthcare documents
  * Create application to parse healthcare standard documents, validate, parse and      store relevant information
  * Maintain legacy applications, handle bugfixes, new development on legacy applications 
  * Handle deployment, git rebases, merges 
  * Languages Used: Java, VisualBasic, ASP, .Net Core, C#, Python, Javascript 
  *	Responsible for development and support of healthcare information technology software and applications.
  *	Expert in analysis and parsing of health EDI specification documents
  * Used JMS publish/subscribe model to interact with other systems asynchronously. The revenue data was pushed to multiple systems using the JMS model. 
  * Acquired hands on experience on using MAVEN, JSF, JQuery, JSP, JAVASCRIPT, IntelliJ, Jetty, Tomcat, .Net Framework Applications
  * Created application to extract data from PDFs using Apache PDF box and GUICE 
  * Used Quartz Scheduler to implement the Nightly Batch jobs and created test UI to trigger the Batch job manually for testing/debugging purpose in Dev region.
  * Acquired hands on experience on GUI development using GWT, .NET Core razor pages, Javascript
  * Create multiple applications to provide accurate and timely creation of vendor specific files.
  * Create drools rules and provide clients flexibility in designing rules based matching systems.
  * Implemented JAVAMail API to send the summary reports to the Business vendors group after the completion of Nightly Batch cycle.
  * Wrote Shared Interface APIs implementing Singleton, Factory and Builder Design Pattern that perform CRUD operations consuming web services.
  * Wrote .Net applications that ingest multiple file formats including xml, csv, text data based on clients lockbox settings
  * Created CI/CD scripts in gitlab that provides automated deployments
  * Wrote test cases/suites and logs for multiple applications using JUNIT and Log4J.Experience with databases such as oracle, MS SQL Server for managing tables, Stored Procedures, views, functions, triggers and packages.
  * Bug fixing, Assisting tech support, Performance monitoring and maintenance, of deployed application in PROD region.
  * Participated in Multifunctional team, Worked with Devops team on deployment and automations, Participated in code review and Everyday Agile Meetings.
  """

[[experience]]
  title = "Data Analyst"
  company = "University of Central Oklahoma"
  company_url = "https://www.uco.edu/student-resources/federal-education-programs/sss/"
  location = "Edmond"
  date_start = "2016-01-11"
  date_end = "2017-12-16"
  description = """
  Supported various grant system at UCO, developed and maintain the various student management system, reporting modules at UCO grants. Handled grant database, report creations and federal submission of mandatory grant reports. Ensure data integrity, validity and provide support to non-technical staff

  Responsibilities include:
  
  *	Writing and configuring code to meet user interface specifications.
  *	Developing and executing test plans (Unit testing).
  *	Create data pipelines for extracting and sanitizing student data
  *	Create auto report populator using the sanitized data
  *	Verification and submission of student data reports to University and Grant Directors
  *	Write utilities to auto extract information for Ellucian Banner Systems
  *	Form Design and data collection using Qualtrics Survey System, the data fed is nightly refreshed with the aggregate student data, providing valuable insights to Academic Advisors and Grant Directors
  *	Created GUI interface for multiple grants data Entry
  *	Edit Grant website, fix lower level bugs, update website periodically
  *	Report core bugs to Ellucian team, work with Grant Controller and DBAs in designing auto pipelines to extract data from Ellucian systems

  """
  
[[experience]]
  title = "Graduate Fellow (Intern)"
  company = "SAXUM"
  company_url = "https://saxum.com/"
  location = "Oklahoma City"
  date_start = "2017-06-10"
  date_end = "2017-12-16"
  description = """
  Responsibilities include:

  * Website development using wordpress in LAMP/WAMP stack.
  * Manual testing of websites. 
  * Website updates, content moderation, SEO
  * Languages used: PHP, Python, Wordpress 
  """  
  
[[experience]]
  title = "Software Developer"
  company = "MixERP Inc"
  company_url = "https://mixerp.org/"
  location = "Kathmandu"
  date_start = "2015-02-10"
  date_end = "2015-12-10"
  description = """
  Developed cost-effective information technology solutions by creating new, and modifying existing, software applications. Analyzed complex systems requirements, existing business processes, and information systems. Code, test, and debug documents, and implemented software applications. 

  Responsibilities include:
  *	Responsible for analyzing business requirements and detail design of the software.
  *	Design and developed Front End User interface. Worked with ASP.NET webforms and other javascripts framework to develop and design various workflow and template mechanism.
  *	Followed Agile Methodology (TDD, SCRUM) to satisfy the customers and wrote XUnit test cases for unit testing the integration layer.
  *	Used log4net for tracking errors and debugging the code.
  *	Involved with project manager in creating detailed project plans.
  *	Designed technical documents using UML.
  *	Involved in developing presentation layer using Webforms, AJAX, and JavaScript.
  *	Responsible for implementing DAO, POCO using Entity Framework.
  *	Worked with business users to create screen mockup and creating technical design documents.

  """ 
  
[[experience]]
  title = "Software Developer"
  company = "Planet Earth Solutions Pvt. Ltd."
  company_url = "https://planetearthsolution.com/"
  location = "Kathmandu"
  date_start = "2015-01-15"
  date_end = "2013-12-16"
  description = """
  Developed banking solutions by creating new, and modifying existing, software applications. Code, test, and debug documents, and implemented software applications. 

  Responsibilities include:
  * Design and developed Front End User interface. Worked with ASP.NET webforms and Javascript frameworks
  *	Updated GUI design to use bootstrap CSS to make it mobile friendly
  *	Created business notification system using SIGNALR and postgres notify
  *	Created Database modules like tables, stored procedures, views and functions to support application infrastructure
  *	Added multiple UI features to quickly create any kind of webforms
  *	Designed the database layer and business layer and aided business in requirement analysis
  *	Worked on various modules, user authentication, management, reporting 
  *	Refactored code to convert applications to 3 tier architecture

  """ 

+++
